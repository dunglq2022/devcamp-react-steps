import { Component } from "react";

class Array extends Component{
    render(){
        let {arraySteps} = this.props
        return(
            arraySteps.map((element, index) => {
                return (
                    <ul key={index}>
                        <li>Bước {element.id}. {element.title}: {element.content}</li>
                    </ul>
                )
            })
        )
    }
}
export default Array;